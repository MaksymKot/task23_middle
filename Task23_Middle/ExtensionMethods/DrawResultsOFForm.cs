﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Middle.ExtensionMethods
{
    public static class DrawResultsOFForm
    {
        public static MvcHtmlString DrawResults(this HtmlHelper html,
            List<(string name, string genre, bool wantScifi, 
                bool wantMobileVersion)> data)
        {
            var numPeopeCertainGenre = new TagBuilder("ul");
            numPeopeCertainGenre.SetInnerText("Number of people, which prefer a certain genre:");
            var utopiaCount = data
                .Where(x => x.genre.Equals("Utopian fiction"))
                .Count();
            var utopidText = new TagBuilder("li");
            utopidText.SetInnerText($"Utopian fiction: {utopiaCount}");
            numPeopeCertainGenre.InnerHtml += utopidText.ToString();

            var histText = new TagBuilder("li");
            var histCount = data
                .Where(x => x.genre.Equals("Historical fiction"))
                .Count();
            histText.SetInnerText($"Historical fiction: {histCount}");
            numPeopeCertainGenre.InnerHtml += histText.ToString();

            var crimeNovelText = new TagBuilder("li");
            var crimeNovelCount = data
                .Where(x => x.genre.Equals("Crime novel"))
                .Count();
            crimeNovelText.SetInnerText($"Crime novel: {crimeNovelCount}");
            numPeopeCertainGenre.InnerHtml += crimeNovelText.ToString();

            var adventureText = new TagBuilder("li");
            var adventureCount = data
                .Where(x => x.genre.Equals("Adventure"))
                .Count();
            adventureText.SetInnerText($"Adventure: {adventureCount}");
            numPeopeCertainGenre.InnerHtml += adventureText.ToString();

            var whatPeopleWantText = new TagBuilder("ul");
            whatPeopleWantText.SetInnerText("What people want:");

            var wantSciFiCount = data.Where(x => x.wantScifi)
                .Count();

            var SciFiText = new TagBuilder("li");
            SciFiText.SetInnerText($"{wantSciFiCount} people want more science fiction books");
            whatPeopleWantText.InnerHtml += SciFiText.ToString();

            var wantMobileVersionCount = data.Where(x => x.wantMobileVersion)
                .Count();

            var mobileVersText = new TagBuilder("li");
            mobileVersText.SetInnerText($"{wantMobileVersionCount} people want a mobile version of this site");
            whatPeopleWantText.InnerHtml += mobileVersText.ToString();

            var output = numPeopeCertainGenre.ToString() + whatPeopleWantText.ToString();

            return new MvcHtmlString(output);
        }
    }
}