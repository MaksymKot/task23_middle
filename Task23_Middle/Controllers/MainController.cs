﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Middle.Controllers
{
    public class MainController : Controller
    {
        public ActionResult MainPage()
        {
            return View(HttpContext.Application["articles"]);
        }
    }
}