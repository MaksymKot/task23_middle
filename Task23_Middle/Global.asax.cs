﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Task23_Middle
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public List<(string name, string dateOfPublish, string text)> articles =
            new List<(string name, string dateOfPublish, string text)>()
            {
                ("A Song of Ice and Fire",
                DateTime.Now.ToShortDateString(),
                @"A Song of Ice and Fire is a series of epic fantasy novels by 
                the American novelist and screenwriter George R. R. Martin. He 
                began the first volume of the series, A Game of Thrones, in 
                1991, and it was published in 1996"),

                ("Ivanhoe",
                DateTime.Now.ToShortDateString(),
                @"Ivanhoe: A Romance by Walter Scott is a historical novel 
                published in three volumes, in 1819, as one of the Waverley 
                novels. At the time it was written, the novel represented a 
                shift by Scott away from writing novels set in Scotland in the 
                fairly recent past to a more fanciful depiction of England in 
                the Middle Ages. Ivanhoe proved to be one of the best-known and 
                most influential of Scott's novels. "),

                ("Macbeth",
                DateTime.Now.ToShortDateString(),
                @"Macbeth is a tragedy by William Shakespeare; it is thought to
                have been first performed in 1606. It dramatises the damaging 
                physical and psychological effects of political ambition on 
                those who seek power for its own sake.")
            };

        public List<(string name, string dateOfPublish, string text)> reviews =
            new List<(string name, string dateOfPublish, string text)>()
            {
                ("Bob", DateTime.Now.ToShortDateString(), "An extremely nice story about Harry Potter"),
                ("Jack", DateTime.Now.ToShortDateString(), "I give \"Crime and Punishment\" 5 stars"),
                ("John", DateTime.Now.ToShortDateString(), "\"A song of Ice and Fire\" is amazing")
            };

        public List<(string name, string genre, bool wantScifi,
                bool wantMobileVersion)> forms = new List<(string name,
                string genre, bool wantScifi, bool wantMobileVersion)>();

        protected void Application_Start()
        {
            Application["articles"] = articles;
            Application["reviews"] = reviews;
            Application["forms"] = forms;

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
